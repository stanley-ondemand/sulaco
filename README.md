# ~*~*~ SULACO ~*~*~ #

### What is it? ###

* Collection of Polymer Custom Elements with styling specific to the Stanley Gibbons brand

### How do I get set up? ###

* Clone the repo, run `bower install`
* If you want to preview the existing elements, simply run `polyserve`
    * If you don't have polyserve, you can run `npm install -g polyserve`
* You can then navigate to localhost:<PORT>/components/sg-elements/<ELEMENT-NAME> to view the documentation page and demo
    * To view docs for the whole project navigate to localhost:<PORT>/components/sg-elements


### Testing

* Install Web Component Tester by running `npm install -g web-component-tester`
    * You may need to run `chmod g+rw ~/.config/configstore/bower-github.yml` if you get the related permissions error when trying to install. If that doesn't work, try running `chmod 755 ~/.config/configstore`
* To run the tests use `wct` from the project root
    * You may need to run `sudo wct` if you get permissions errors
    * To run the tests for a given element, use `wct <ELEMENT-NAME>/test/index.html`
        * Individual tests need to be run from project root in order for dependencies to be served up correctly


### Adding an element

* If you want to add your own element or behavior, you can start by copying the folder of an existing element and making the necessary changes
    * Don't forget to add your element to the global import file (sg-elements.html), and add your element's main test file to the project test suite file (test/index.html)
* These elements are originally based on the [Polymer Seed Element](https://github.com/PolymerElements/seed-element)